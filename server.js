const fs = require("fs");
const path = require("path");
const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const { makeExecutableSchema } = require("graphql-tools");

const schemaFile = path.join(__dirname, "schema.graphql");
const typeDefs = fs.readFileSync(schemaFile, "utf8");

const orderDetails = JSON.parse(
  fs.readFileSync(path.join(__dirname, "./orders.json"))
);
const lineItems = JSON.parse(
  fs.readFileSync(path.join(__dirname, "./line-items.json"))
);

const resolvers = {
  Query: {
    getOrder: (_, { orderId }) => {
      console.log("Fetching leve 1 OrderId:" + orderId);
      return { orderId: orderId };
    },
  },
  OrderDetail: {
    order: (parent) => {
      console.log("Fetching level 2 Order Details:" + parent.orderId);
      return orderDetails.find((order) => order.orderId === parent.orderId);
    },
    lineItem: (parent) => {
      console.log("Fetching level 2 Line Details:" + parent.orderId);
      return lineItems.find((order) => order.orderId === parent.orderId);
    },
  },
};

const schema = makeExecutableSchema({ typeDefs, resolvers });
var app = express();

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    graphiql: true,
  })
);
app.listen(4000);
console.log("Running a GraphQL API server at localhost:4000/graphql");
